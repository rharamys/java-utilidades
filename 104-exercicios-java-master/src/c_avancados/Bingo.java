package c_avancados;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 * Crie um programa que realiza o sorteio de 20 números
 * de 1 à 50, de forma que um número sorteado não pode ser repetido.
 * 
 * Após o sorteio, o progama deve conferir uma cartela pré-determinada
 * e determinar o resultado dentre as possíveis opções:
 * 
 * - Bingo: cartela cheia
 * - Linha: linha cheia (vertical ou horizontal)
 * - Erro: nenhuma das anteriores
 *
 * O programa deve imprimir os números sorteados e o resultado.	
 */
public class Bingo {

	public static void main(String args[]) {
		int[][] cartela = {
			{10, 21, 34, 43},
			{7,  15, 11, 50},
			{41,  9, 33,  2},
			{1,   2, 34, 49}
		};
		
		ArrayList<Integer> numerosDisponiveis = new ArrayList<>();
		for (int i=1; i<=50; i++) {
			numerosDisponiveis.add(i);
		}
		
		ArrayList<Integer> numerosSorteados = new ArrayList<>();
		int marker = 0;
		for (int i=1; i<21; i++) {
			marker = (int)Math.ceil(Math.random()*numerosDisponiveis.size() -1);
			numerosSorteados.add(numerosDisponiveis.get(marker));
			numerosDisponiveis.remove(marker);
		}
		
		// FALTA IMPLEMENTAR A CHECAGEM DE PONTUACAO DO BINGO E A IMPRESSAO
		
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i<numerosSorteados.size(); i++) {
			builder.append(numerosSorteados.get(i) + ", ");
		}
		builder.deleteCharAt(builder.length()-2);
		System.out.println(builder);
		
		ArrayList<HashMap> matches = new ArrayList<>();
		HashMap<String,Integer> hashMapMatch = new HashMap<String,Integer>();
		
		int[] xMatches = new int[4];
		int[] yMatches = new int[4];
		
		for (int i=0 ; i<4; i++){
			for (int j=0; j<4; j++) {
				if (numerosSorteados.contains(cartela[i][j])) {
					xMatches[i] += 1;
					yMatches[j] += 1;
				}
			}
		}
		
		boolean horizMatch = false;
		boolean vertMatch  = false;
		boolean bingo = true;
		
		for (int i=0 ; i<4; i++){
			for (int j=0; j<4; j++) {
				if (xMatches[i] == 4) {
					horizMatch = true;
				} else {
					bingo = false;
				}
				if (yMatches[j] == 4) {
					vertMatch = true;
				} else {
					bingo = false;
				}
			}
		}
		
		if (horizMatch == true) {
			if (bingo == true) {
				System.out.println("Bingo!");
			} else {
				System.out.println("Linha!");
			}
		} else if (vertMatch == true) {
			System.out.println("Linha!");
		} else {
			System.out.println("Errouuuuuuu!");
		}
	}
}
