package c_avancados;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Crie um programa que faça um saque no arquivo contas.csv. O programa
 * deve receber um id e o valor do saque. Ele deve informar caso o id
 * não seja encontrado ou caso o valor não esteja disponível.
 * 
 * Após o saque, o arquivo deve ser atualizado.
 * 
 */
public class Saque {
	public static void main(String[] args) {
		
		System.out.println("Insira um ID e o svalor para saque separados por virgula:");
		Scanner scanner = new Scanner(System.in);
		String stringInputado = scanner.nextLine();
		scanner.close();
		
		try {
			int idUsuario = Integer.parseInt(stringInputado.split(", ")[0]);
			try {
				double valor = Double.parseDouble(stringInputado.split(", ")[1]);
				
				Path path = Paths.get("src/contas.csv");
				List<String>linhas=Files.readAllLines(path);
				Path pathWrite = Paths.get("src/contasNovo.csv");
				boolean achou = false;
				for (int i=0; i < linhas.size() ; i++) {
					if (i>0) {
						String[] registro = linhas.get(i).split(",");
						if (Integer.parseInt(registro[0]) == idUsuario) {
							achou = true;
							double saldoAnterior = Integer.parseInt(registro[4]);
							if (saldoAnterior >= valor) {
								double saldoAtual = saldoAnterior - valor;
								DecimalFormat df = new DecimalFormat("0");
								String registroNovo = new String(registro[0] + "," + registro[1] + "," + registro[2] + "," + registro[3] + "," + df.format((saldoAtual)));
								linhas.remove(i);
								linhas.add(i, registroNovo);
								Files.write(pathWrite, linhas);
							} else {
								System.out.println("O usuário não tem saldo.");
							}
						}
					}
				}
				if (!achou) {
					System.out.println("O usuário não foi encontrado");
				}
			} catch (Exception e) {
				System.out.println("Valor inválido.");
			}
		} catch (Exception e) {
			System.out.println("ID inválido.");
		}		
	}
}
