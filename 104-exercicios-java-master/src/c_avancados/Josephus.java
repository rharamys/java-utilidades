package c_avancados;

import java.text.DecimalFormat;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * O problema de Josephus:
 * Josephus foi convidado para participar de um programa de auditório. Nesse
 * programa, os participantes convidados devem formar um círculo. O apresentador
 * remove os participantes do círculo de forma alternada: o primeiro fica no
 * jogo, o segundo sai, o terceiro fica, o quarto sai, e assim sucessivamente.
 *
 * Como Josephus tem um raciocínio muito rápido, ele contou a quantidade de
 * convidados e se posicionou no círculo na posição correta, e venceu o jogo,
 * ganhando o grande prêmio do programa!
 *
 * Escreva um programa que resolva o 'problema de Josephus'. Dado um número N
 * de participantes ordenados em círculo, determine qual a posição em que alguém
 * deve ficar para vencer o jogo.
 *
 * O número N de participantes é lido da entrada padrão e a posição correta do
 * vencedor é impressa na saída padrão.
 *
 */
public class Josephus {
	public static void main(String[] args) {

		System.out.println("Insira um número inteiro:");
		Scanner scanner = new Scanner(System.in);
		String stringInputado = scanner.nextLine();
		scanner.close();
		
		try {
			int numero = Integer.parseInt(stringInputado);
			System.out.println(josephus(numero));
		} catch (Exception e) {
			System.out.println("Insira somente número inteiro.");
		}
	}

	public static int josephus(int num) {
		
		ArrayList<Integer> pessoas = new ArrayList<Integer>();
		for (int i=0;i<num;i++) {
			pessoas.add(i+1);
		}
		
		boolean saiDoJogo = false;
		int i = 0;
		boolean aux = false;
		
		while(pessoas.size()>1) {
			
			if (saiDoJogo == true) {
				pessoas.remove(i);
				i--;
			}
			
			i++;	
			
			if (i == pessoas.size()) {
				i=0;
			}
			
			saiDoJogo = !saiDoJogo;
		
		}
		
		return pessoas.get(0);
	}
}