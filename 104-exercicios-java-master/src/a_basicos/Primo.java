package a_basicos;

/**
 * Crie um programa que imprima todos os números
 * primos de 1 à 100
 */
public class Primo {
public static void main(String[] args) {
		
		for (int i=1; i< 101 ; i++) {
			if (primo(i)) {
				System.out.println(i);
			}
		}
		
	}

	public static boolean primo( int num) {
		
		for (int i = num-1; i >1 ; i--) {
			if (num % i == 0) {
				return false;
			}
		}
		
		if (num == 1) {
			return false;
		}
		
		return true;
	}
}
