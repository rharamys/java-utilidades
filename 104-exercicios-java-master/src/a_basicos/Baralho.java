package a_basicos;

/**
 * Crie um programa que imprima todas as cartas do baralho.
 * 
 * Exemplo: 
 * 
 * Ás de Ouros
 * Ás de Espadas
 * Ás de Copas
 * Ás de Paus
 * Dois de Ouros
 * ...
 * 
 */
public class Baralho {
	public static void main(String[] args) {
		String numero = new String();
		String naipe = new String();
		String[] naipes = {"Ouros", "Espadas", "Copas", "Paus"};
		String[] numeros = {"Ás", "Dois", "Três", "Quatro", "Cinco", "Seis", "Sete", "Oito", "Nove", "Dez", "Valete", "Dama", "Rei"};
		for (Integer i=1; i <14 ; i++ ) {
			for (int j=1; j<5; j++) {
				numero = numeros[i-1];
				naipe = naipes[j-1];
				System.out.println(numero + " de " + naipe);
			}
		}
	}
}
