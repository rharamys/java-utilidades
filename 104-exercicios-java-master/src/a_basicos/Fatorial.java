package a_basicos;

import java.util.Scanner;

/**
 * Crie um programa que recebe um número do usuário
 * e imprime a fatorial desse mesmo número.
 * 
 */
public class Fatorial {
	public static void main(String[] args) {
		System.out.println("Digite um número para cálculo de fatorial:");
		Scanner scanner = new Scanner(System.in);
		String numeroInputado = scanner.nextLine();
		scanner.close();
		try {
			int number = Integer.parseInt(numeroInputado);
			int fatorial = 1;
			for (int i= number; i > 1 ; i--) {
				fatorial += fatorial*(i-1);
				System.out.println(fatorial);
			}
			System.out.println("O Fatorial é " + fatorial);
		} catch (Exception e) {
			System.out.println("Digite um número!");
		}			
	}
}
