package a_basicos;

import java.util.Scanner;

/**
 * Crie um programa que recebe as três dimensões de uma piscina
 * (largura, comprimento, profundidade) e retorne a sua capacidade
 * em litros.
 */
public class Piscina {
	public static void main(String[] args) {
		System.out.println("Digite largura, profundidade e comprimento em metros e separado por virgulas");
		Scanner scanner = new Scanner(System.in);
		String stringInputada = scanner.nextLine();
		scanner.close();
		String[] dimensoes = stringInputada.split(", ");
		try {
			System.out.println("O volume é " + calculaVolume(Integer.parseInt(dimensoes[0]),Integer.parseInt(dimensoes[1]),Integer.parseInt(dimensoes[2])));
		} catch(Exception e) {
			System.out.println("Digite apenas números!");
		}
	}
	
	public static double calculaVolume(int a, int b, int c) {
		return a*b*c*1000;
	}
}
