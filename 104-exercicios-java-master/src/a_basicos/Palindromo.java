package a_basicos;

import java.util.Scanner;

/**
 * Crie um programa que recebe uma palavra do terminal e determina
 * se ela é um palíndromo.
 * 
 * Exs: 
 * 
 * input: ovo
 * output: A palavra ovo é um palíndromo
 * 
 * input: jose
 * output: A palavra jose não é um palíndromo 
 */
public class Palindromo {
	public static void main(String[] args) {
		System.out.println("Digite uma palavra:");
		Scanner scanner = new Scanner(System.in);
		String palavraInputada = scanner.nextLine();
		scanner.close();
		
		if(palindromo(palavraInputada)) {
			System.out.println("Palavra é palíndromo");
		} else {
			System.out.println("Palavra não é palíndromo");
		}
	}
	
	public static boolean palindromo(String palavra) {
		char[] chars = palavra.toCharArray();
		for (int i= 0; i < palavra.length() ; i++) {
			if (chars[i] != palavra.charAt(palavra.length()-1-i)) {
				return false;
			}
		}
		return true;
	}
}
