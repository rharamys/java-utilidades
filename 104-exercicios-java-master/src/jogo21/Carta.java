package jogo21;

public class Carta {
	private Naipe naipe;
	private Numero numero;
	
	public Carta(Naipe naipe, Numero numero) {
		this.naipe = naipe;
		this.numero = numero;
	}
	
	public Numero getNumero() {
		return this.numero;
	}
	
	@Override
	public String toString() {
		return numero.toString() + " de " + naipe.toString(); 
	}
}
