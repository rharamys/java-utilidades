package jogo21;

import java.util.ArrayList;

public class Jogador {
	
	private ArrayList<Carta> cartasDaMao = new ArrayList<>();
	private boolean ativo = true;
	
	public void adicionarCarta(Carta carta) {
		cartasDaMao.add(carta);
	}
	
	public void zerarCartas() {
		cartasDaMao.clear();
	}
	
	public int contarPontos() {
		int pontos = 0;
		ArrayList<Carta> cartasAs = new ArrayList<>();
		for(int i=0;i<cartasDaMao.size();i++) {
			switch(cartasDaMao.get(i).getNumero()) {
				case A: cartasAs.add(cartasDaMao.get(i));	break;
				case DOIS: 		pontos += 2; 				break;
				case TRES:		pontos += 3; 				break;
				case QUATRO: 	pontos += 4; 				break;
				case CINCO: 	pontos += 5; 				break;
				case SEIS: 		pontos += 6; 				break;
				case SETE: 		pontos += 7; 				break;
				case OITO: 		pontos += 8; 				break;
				case NOVE: 		pontos += 9; 				break;
				case DEZ: 		pontos += 10; 				break;
				case J: 		pontos += 10; 				break;
				case Q: 		pontos += 10; 				break;
				case K: 		pontos += 10; 				break;
			}
		}
		if (!cartasAs.isEmpty()) {
			int pontosAs = 0;
			for (int i=0;i < cartasAs.size();i++) {
				pontosAs += 11;
			}
			if (pontos + pontosAs > 21) {
				pontosAs = 11;
				for (int i=0;i < cartasAs.size()-1;i++) {
					pontosAs += 1;
				}
				if (pontos + pontosAs > 21) {
					pontosAs -= 10;
				}
			}
			pontos += pontosAs;
		}
		return pontos;
	}
	
	public String toString() {
		StringBuilder builder = new StringBuilder();
		for (int i=0; i<cartasDaMao.size(); i++) {
			builder.append(cartasDaMao.get(i).toString() + ", \n");
		}
		builder.delete(builder.length()-3,builder.length()-1);
		return builder.toString();
	}
	
	public boolean getAtivo() {
		return this.ativo;
	}
	
	public void parou() {
		this.ativo = false;
	}
}
