package jogo21;

import java.util.Scanner;

public class App {

	public static void main(String[] args) {

		Baralho baralho = new Baralho();
		
		System.out.println("Jogador 1:");
		int pontos1 = Controlador.jogar(baralho);
		
		System.out.println("=====================================");
		System.out.println("Jogador 2:");
		int pontos2 = Controlador.jogar(baralho);
		
		System.out.println("=====================================");
		System.out.println("Jogador 1: " + pontos1);
		System.out.println("Jogador 2: " + pontos2);
		
		boolean jogador1Qualificado = true;
		boolean jogador2Qualificado = true;
		
		if (pontos1 > 21) {
			jogador1Qualificado = false;
		}
		
		if (pontos2 > 21) {
			jogador2Qualificado = false;
		}
		
		if (jogador1Qualificado && jogador2Qualificado) {
			if (pontos1 > pontos2) {
				System.out.println("O jogador 1 ganhou!");
			} else  if ( pontos1 == pontos2 ){
				System.out.println("EMPATE!");
			} else {
				System.out.println("O jogador 2 ganhou!");
			}
		} else if (!jogador1Qualificado && !jogador2Qualificado) {
			System.out.println("EMPATE!");
		} else if (jogador1Qualificado) {
			System.out.println("O jogador 1 ganhou!");
		} else {
			System.out.println("O jogador 2 ganhou!");
		}
	}
}
