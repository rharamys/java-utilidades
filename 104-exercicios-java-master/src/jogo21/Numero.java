package jogo21;

public enum Numero {
	A("As"),
	DOIS("Dois"),
	TRES("Tres"),
	QUATRO("Quatro"),
	CINCO("Cinco"),
	SEIS("Seis"),
	SETE("Sete"),
	OITO("Oito"),
	NOVE("Nove"),
	DEZ("Dez"),
	J("Valete"),
	Q("Dama"),
	K("Rei");
	
	private String descricao;
	
	private Numero(String descricao) {
		this.descricao = descricao;
	}
	
	public String toString() {
		return descricao;
	}
}
