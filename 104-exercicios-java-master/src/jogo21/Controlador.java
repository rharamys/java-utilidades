package jogo21;

import java.util.Scanner;

public class Controlador {

	public static int jogar(Baralho baralho) {
		Jogador jogador = new Jogador();
		jogador.adicionarCarta(baralho.retirarCarta());
		jogador.adicionarCarta(baralho.retirarCarta());
		System.out.println(jogador.toString());
		
		do {
			System.out.println("Você tem " + jogador.contarPontos() + " pontos. O que deseja fazer?");
			System.out.println("1 - Retirar carta. 2- Parar.");
			Scanner scanner = new Scanner(System.in);
			String stringInputado = scanner.nextLine();
			if (stringInputado.equals("1")) {
				jogador.adicionarCarta(baralho.retirarCarta());
				System.out.println(jogador.toString());
				if (jogador.contarPontos() > 21) {
					System.out.println("Você foi desqualificado com " + jogador.contarPontos() + " pontos.");
				} else {
					if (jogador.contarPontos() == 21) {
						System.out.println("Você conseguiu 21!!!");
					}
				}
			} else {
				System.out.println("Você fez " + jogador.contarPontos() + " pontos. Parabéns!");
				jogador.parou();
			}
		} while(jogador.contarPontos() < 21 && 
			    jogador.getAtivo() == true);
		return jogador.contarPontos();
	}
}
