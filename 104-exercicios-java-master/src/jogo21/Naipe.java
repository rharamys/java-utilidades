package jogo21;

public enum Naipe {
	COPAS("Copas"),
	PAUS("Paus"),
	ESPADAS("Espadas"),
	OUROS("Ouros");
	
	private String descricao;
	
	private Naipe(String descricao) {
		this.descricao = descricao;
	}
	
	public String toString() {
		return descricao;
	}
}
