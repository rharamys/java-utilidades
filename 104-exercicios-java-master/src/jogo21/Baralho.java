package jogo21;

import java.util.ArrayList;

public class Baralho {
	
	ArrayList<Carta> cartas = new ArrayList<>();
	
	public Baralho() {
		for (Numero numero : Numero.values()) {
			for (Naipe naipe : Naipe.values()) {
				cartas.add(new Carta(naipe,numero));
			}
		}
		embaralhar();
	}
	
	public void embaralhar() {
		ArrayList<Carta> embaralhado = new ArrayList<>();
		for (int i=0; i<52; i++) {
			int marker = (int)Math.ceil(Math.random()*cartas.size() -1);
			embaralhado.add(cartas.get(marker));
			cartas.remove(marker);
		}
		this.cartas = embaralhado;
	}

	public Carta retirarCarta() {
		Carta carta = cartas.get(cartas.size()-1);
		cartas.remove(cartas.get(cartas.size()-1));
		return carta;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		for (int i=0; i<cartas.size(); i++) {
			builder.append(cartas.get(i).toString() + ", \n");
		}
		builder.delete(builder.length()-4,builder.length()-1);
		return builder.toString();
	}
	
}
