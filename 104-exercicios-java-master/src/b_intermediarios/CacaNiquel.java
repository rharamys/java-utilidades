package b_intermediarios;

/**
 * Crie um programa que simula o funcionamento de um caça níquel, da seguinte forma:
 * 
 * 1- Sorteie 3x um valor dentre as strings do vetor de valores
 * 2- Compare os valores sorteados e determine os pontos obtidos usando o
 * seguinte critério:
 * 
 * Caso hajam três valores diferentes: 0 pontos
 * Caso hajam dois valores iguais: 100 pontos
 * Caso hajam três valores iguais: 1000 pontos
 * Caso hajam três valores "7": 5000 pontos
 * 
 */
public class CacaNiquel {
	public static void main(String args[]) {
		String[] valores = {"abacaxi", "framboesa", "banana", "pera", "7"};
		String[] obtidos = new String[3]; 
		for (int i=0; i<3; i++) {
			obtidos[i] = valores[(int) Math.ceil(Math.random()*5 -1)];
		}
		int pontuacao;
		if (obtidos[0].equals(obtidos[1]) && obtidos[0].equals(obtidos[2])) {
			if (obtidos[0].equals("7")) {
				pontuacao = 5000;
			} else {
				pontuacao = 1000;
			}
		} else if (obtidos[0].equals(obtidos[1]) || obtidos[1].equals(obtidos[2]) || 
				   obtidos[0].equals(obtidos[2])) {
			pontuacao = 100;
		} else {
			pontuacao = 0;
		}
		System.out.println(obtidos[0] + " | " + obtidos[1] + " | " + obtidos[2]);
		System.out.println("Você fez " + pontuacao + " pontos.");
	}
}
