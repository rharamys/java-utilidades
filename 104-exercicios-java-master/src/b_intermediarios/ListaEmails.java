package b_intermediarios;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Crie um programa que lê os clientes presentes no arquivo contas.csv e 
 * filtre aqueles que possuem o saldo superior à 7000.
 * Desses clientes, criar outro arquivo de texto que possua todos os clientes
 * no seguinte formato:
 * 
 * João da Silva<joaosilva@teste.com>, Maria da Penha<maria@teste.com>
 * 
 */
public class ListaEmails {
	public static void main(String[] args) {
		Path path = Paths.get("src/contas.csv");
		try {
			List<String>linhas=Files.readAllLines(path);
			Path pathWrite = Paths.get("novoArq.txt");
			ArrayList<String> registros = new ArrayList<>();
			for (int i=0; i < linhas.size() ; i++) {
				if (i>0) {
					String[] registro = linhas.get(i).split(",");
					if (Integer.parseInt(registro[4]) > 7000) {
						String dadosWrite = registro[1] + " " + registro[2] + "<" + registro[3] + ">";
						registros.add(dadosWrite);
					}
				}
			}
			if (!registros.isEmpty()) {
				StringBuilder builder = new StringBuilder();
				for(String registro : registros) {
					builder.append(registro + ", ");
				}
				builder.delete(builder.length()-2, builder.length());
				builder.append(".");
				Files.write(pathWrite, builder.toString().getBytes());
			}
		} catch (Exception e) {
			System.out.println("Deu erro na leitura!");
		}
	}
}
