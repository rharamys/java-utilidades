package b_intermediarios;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

/**
 * Crie um programa que recebe um todo list e a escreve em um arquivo txt.
 * 
 * A cada iteração, o sistema deve pedir uma  única tarefa e exibir a possibilidade
 * de encerrar o programa.
 * 
 * Quando o usuário encerrar o programa, deve-se gerar o arquivo txt com as tarefas
 * que foram inseridas.
 *
 */
public class ListaTarefas {
	public static void main(String[] args) {
		System.out.println("Digite uma tarefa ou tecle 'x' para sair:");
		Scanner scanner = new Scanner(System.in);
		String stringInputado = scanner.nextLine();

		StringBuilder builder = new StringBuilder();
		int counter = 0;
		
		while (!stringInputado.equals("x")) {
			counter ++;
			builder.append(stringInputado + "; ");
			System.out.println("Digite uma tarefa ou tecle 'x' para sair:");
			stringInputado = scanner.nextLine();
		}
		
		if (counter > 0) {
			builder.delete(builder.length()-2, builder.length());
			Path pathWrite = Paths.get("tarefas.txt");
			try {
				Files.write(pathWrite, builder.toString().getBytes());
			} catch (IOException e) {
				System.out.println("Deu ruim!!!");
			}
		}
		
	}
}
