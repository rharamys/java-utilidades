package b_intermediarios;

import java.util.Scanner;

/**
 * Dado um numero inteiro positivo, encontre 
 * a menor soma de dois números cujo produto
 * seja igual ao número
 * 
 * Ex: se n = 10, então resultado = 7, pois 5 * 2 
 * é o menor produto inteiro positivo que resulta
 * 10.
 * 
 * Testes:
 * 
 */
public class ComplexidadeInteiros {
	public static void main(String[] args) {
		System.out.println("Digite um número:");
		Scanner scanner = new Scanner(System.in);
		String stringInputado = scanner.nextLine();
		scanner.close();
		try {
			int numeroInputado = Integer.parseInt(stringInputado);
			int menorNumeroCalc = numeroInputado;
			int menorNumero1 = 1;
			int menorNumero2 = numeroInputado;
			for (int i=1; i < numeroInputado ; i++) {
				if (numeroInputado%i ==0) {
					int j = numeroInputado / i;
					int calculo = j+i;
					if (calculo < menorNumeroCalc) {
						menorNumeroCalc = calculo;
						menorNumero1 = i;
						menorNumero2 = j;
					}
				}
			}
			System.out.println("Os menores numeros são " + menorNumero1 + " e " + menorNumero2 + " com soma de " + menorNumeroCalc);
		} catch (Exception e) {
			System.out.println("Digite um número");
		}
	}
}

