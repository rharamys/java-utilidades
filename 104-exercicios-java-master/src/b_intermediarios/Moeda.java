package b_intermediarios;

import java.text.DecimalFormat;
import java.util.Scanner;

/**
 * Crie um programa que receba um valor decimal e o converta
 * para o formato moeda. Deve-se fazer as devidas validações 
 * para que uma entrada incorreta de dados não interrompa a
 * execução do programa.
 * 
 */
public class Moeda {
	public static void main(String[] args) {
		
		System.out.println("Insira o valor em reais:");
		Scanner scanner = new Scanner(System.in);
		String stringInputado = scanner.nextLine();
		scanner.close();
		
		try {
			if (stringInputado.split("\\.").length > 1 ) {
				String[] stringDecimal = stringInputado.split("\\.");
				if (stringDecimal[1].length() > 2 ) {
					System.out.println("Insira somente duas casas decimais.");
				} else {
					double numero = Double.parseDouble(stringInputado);
					DecimalFormat df = new DecimalFormat("R$ 0.00");
					System.out.println(df.format(numero));
				}
			}
		} catch (Exception e) {
			System.out.println("Insira somente números e no máximo um ponto.");
		}
	}
}
